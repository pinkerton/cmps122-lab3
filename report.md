# CMPS 122 Lab 3 Report

## Diffs

For phase 3, I created a [new public git repository](https://bitbucket.org/pinkerton/cmps122-lab3/commits/all) for my code exactly as it was submitted for phase 1 and created commits for my partial credit and vulnerability fix changes.

I know that git history can be faked, so I'm happy to send over a zip file of the git repository or validate this another way if the graders would like. My last code commit was `716590f` at `Fri Mar 15 22:06:41 2019 -0700`.

## Commands

### Login

`curl -i -X POST "localhost:PORT/login?username=hacker&password=hunter2"`

### Upload a file

`curl -H "Cookie: YOUR_COOKIE" --data-binary @index.html localhost:PORT/hacker/index.html`

### Fetch a file

`curl -H "Cookie: YOUR_COOKIE" localhost:PORT/hacker/index.html`

## Vulnerabilities in my project

The biggest issue with my implementation was that I did not fully implement cookie checking for phase 1. My server issued cookies, but did not use them to authenticate users or block them from accessing/creating files they did not have access to. It came down to the wire when doing the first implementation, and I wasn't crazy enough to think I could implement cookie checking in the 10 minutes before the deadline. This was the first thing I fixed (for partial credit) before addressing other security vulnerabilities. 

One of my other vulnerabilities was around being able to crash the server with a malformed HTTP request that didn't contain a Content-Length header, which I resolved by issuing a `401 Forbidden` response in this case. There is probably a better header to use here, but this was not specified in the project documentation.

There was also a bug where the server failed to create subdirectories when users created files, which I found out was a silly bug and fixed.

Another major security problem was not protecting against relative path attacks where users could use ".." as the username or another part of the path to access or write files they shouldn't have access to. I made a trivial fix for this where I naively look for ".." in the path and return a `401 Forbidden` if it appears at all.

The only report that I think is invalid was the following:

```
Problem3
The Cookie is only checked based on its length, so the attacker can
 forge a valid Cookie as long as it has the required length, for
  example, attakcers can use:

curl -H "Cookie: AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" -X POST 
--data "this is raw data" --path-as-is "http://localhost:10529/user/
file1"

to do uploading without login.

```

My HTTP server does validate cookies *now*, but previously it appeared to only look at the length of the cookie because it actually didn't validate the cookie at all! This was among the first things I addressed.

### Finding vulnerabilities in other projects

To find vulnerabilities in other projects, I created a few tests shell files to issue curl commands that contained inputs that tested a few things like:

 * relative path attacks (..)
 * buffer overflows / crashes with extremely long entries in the `users` file
 * crashes by not including common headers in the HTTP request
 * crashes by including extremely long parameters (username/password/path) in the URLs for login/get/post
 * reading files like `users` that the public should not have access to

 I also always checked the cookie generation code to see if it was vulnerable or used an dangerous algorithm. One example in particular used the user's password as the cookie.

 The biggest challenge was the specification not being strict enough, so I had to modify my tests for almost every project I evaluated.

## Cookies

Based on the project description and Piazza, the server returns a `Set-Cookie` header in the *response* upon successful authentication. 

Subsequent *requests* should include the `Cookie` header.

## Resources

I used [this](https://stackoverflow.com/questions/3855127/find-and-kill-process-locking-port-3000-on-mac) in order to kill my process when it held onto a port after I exited it.

Since we're POSTing files to the webserver, I used [this technique from Piazza](https://piazza.com/class/jpoownog84a7ih?cid=198) for formatting my request:

`curl -F "file=@<src-file>;filename=<dest-file>" localhost:<your-port>/<dest-file>`

To POST things with URL-encoding:

`curl -d "data=test" -X POST localhost:6969/a/b`

Building multi-line strings: [StackOverflow](https://stackoverflow.com/a/797351)

I learned about `memmove` from [this StackOverflow](https://stackoverflow.com/a/4295760).

Initially I had my server return a timestamp, but took that out after reading [this](https://www.tutorialspoint.com/http/http_responses.htm), which someone posted on Piazza.