/*

POST a file to the server with:
curl --data-binary @FILENAME localhost:PORT/uploaded_file

To create a file as a user:

curl -H "Cookie:5PN2qmWqBlQ9wQj99nsQzldVI5ZuGXbE" --data-binary @FILENAME localhost:PORT/hacker/FILENAME

Everything else behaves the same as specified in the assignment.

*/

/**
 * Copyright (C) 2018 David C. Harrison - All Rights Reserved.
 * You may not use, distribute, or modify this code without
 * the express written permission of the copyright holder.
 */
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <netinet/in.h>
#include <string.h>
#include <assert.h>
#include <sys/stat.h>
#include <stdbool.h>

#define BYTES 2048

static const char GET[] = "GET";
static const char POST[] = "POST";
static const char LOGIN_ENDPOINT[] = "login?";

static const char RESPONSE_BODY_404[] = \
"<html>\n"
"<head><title>404 Not Found\n"
"</title></head>\n"
"<body>\n"
"<h1>404 not found</h1>\n"
"</body>\n"
"</html>";

static const char RESPONSE_BODY_401[] = \
"<html>\n"
"<head><title>401 Forbidden\n"
"</title></head>\n"
"<body>\n"
"<h1>401 forbidden</h1>\n"
"</body>\n"
"</html>";

static const char RESPONSE_BODY_POST_OK[] = \
"{\n"
"\tstatus: 'ok'\n"
"}\n";

static const char RESPONSE_BODY_POST_ERR[] = \
"{\n"
"\tstatus: 'error'\n"
"}\n";

static char* constructHTTP404() {
    char *response = malloc(BYTES);
    sprintf(response, \
        "HTTP/1.1 %d %s\r\n"
        "Server: Totally Secure HTTP Server\r\n"
        "Content-Length: %lu\r\n"
        "Content-Type: text/html; charset=iso-8859-1\r\n"
        "Connection: Closed\r\n\r\n"
        "%s\n",
        404, "Not Found",
        strlen(RESPONSE_BODY_404), RESPONSE_BODY_404
    );
    return response;
}

static char* constructHTTP401() {
    char *response = malloc(BYTES);
    sprintf(response, \
        "HTTP/1.1 %d %s\r\n"
        "Server: Totally Secure HTTP Server\r\n"
        "Content-Length: %lu\r\n"
        "Content-Type: text/html; charset=iso-8859-1\r\n"
        "Connection: Closed\r\n\r\n"
        "%s\n",
        401, "Forbidden",
        strlen(RESPONSE_BODY_401), RESPONSE_BODY_401
    );
    return response;
}

static char* constructPOSTResponse(const char body[]) {
    char *response = malloc(BYTES);
    sprintf(response, \
        "HTTP/1.1 %d %s\r\n"
        "Server: Totally Secure HTTP Server\r\n"
        "Content-Length: %lu\r\n"
        "Content-Type: application/json; charset=iso-8859-1\r\n"
        "Connection: Closed\r\n\r\n"
        "%s\n",
        200, "OK",
        strlen(body), body
    );
    return response;
}

static char* constructLoginResponse(const char body[], char* authToken) {
    char *response = malloc(BYTES);
    sprintf(response, \
        "HTTP/1.1 %d %s\r\n"
        "Server: Totally Secure HTTP Server\r\n"
        "Content-Length: %lu\r\n"
        "Content-Type: application/json; charset=iso-8859-1\r\n"
        "Set-Cookie: %s\r\n"
        "Connection: Closed\r\n\r\n"
        "%s\n",
        200, "OK",
        strlen(body),
        authToken,
        body
    );
    return response;
}

// generate a string of random characters
// source: https://stackoverflow.com/a/440240
void genRandom(char *s, const int len) {
    static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";
    for (int i = 0; i < len; ++i) {
        s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    }
    s[len] = 0;
}

static unsigned int getContentLength(char* request) {
    unsigned int contentLength = 0;
    char *remaining = strstr(request, "Content-Length: ");
    if (remaining == NULL) {
        return 0;
    }
    int results = sscanf(remaining, "Content-Length: %d\n", &contentLength);
    if (results != 1) {
        return 0;
    }
    return contentLength;
}

// Parse and return the cookie from the request header
static char* getCookie(char* request) {
    char* cookie = malloc(32);
    char *remaining = strstr(request, "Cookie: ");
    int results = sscanf(remaining, "Cookie: %s\n", cookie);
    if (results != 1) {
        printf("no cookie!");
    }
    return cookie;
}

// check if the passed username/cookie combo is valid
static bool isValidCookie(char *username, char* cookie) {
    assert(username != NULL);
    assert(cookie != NULL);

    FILE* fpc = fopen("cookies", "r");
    if (fpc == NULL) {
        perror("no cookies file found");
    }

    char buffer[258]; // 258 = 128 + 1 + 128 + 1
    while (fgets(buffer, 258, fpc)) {
        char* token = strtok(buffer, ":");
        char* usernameRecord = strndup(token, 128);
        token = strtok(NULL, "\n");
        char* cookieRecord = strndup(token, 128);
        if (strncmp(username, usernameRecord, 128) == 0 &&
            strncmp(cookie, cookieRecord, 32) == 0) {
                fclose(fpc);
                return true;
        }
    }
    fclose(fpc);
    return false;
}

// try to authenticate the username/cookie combo
static bool authenticated(int sock, char *username, char *cookie) {
    assert(username != NULL);
    assert(cookie != NULL);
    if (isValidCookie(username, cookie)) {
        return true;
    }
    char* response = constructHTTP401();
    write(sock, response, strlen(response)-1);
    free(response);
    return false;
}

// pull the username out of the path
static void getUsername(char *username, char *path) {
    char* pathDup = strdup(path);
    char* username2 = strtok(pathDup, "/");
    strncpy(username, username2, 128);
    free(pathDup);
}

// write a file that has been POSTed
static void writeFile(int sock, char *fname, char *body, 
                        unsigned int contentLength, char* cookie) {
    char *username = malloc(128);
    getUsername(username, fname);

    // TODO: check that username != ".."

    if (!authenticated(sock, username, cookie)) {
        free(username);
        return;
    }

    // parse the file path and create intermediate directories
    FILE* fp = fopen(fname, "wb");
    if (fp == NULL) {
        char* path = calloc(strlen(fname), sizeof(char));
        char* fnameDup = strdup(fname);
        char* token = strtok(fnameDup, "/");
        char* lastToken = strdup(token);
        bool first = true;
        while (token != NULL) {
            if (!first) {
                int err = mkdir(path, 0700);
                if (err != 0) {
                    perror("failed to create directory");
                }
            } else {
                first = false;
            }
            lastToken = strdup(token);
            strcat(path, lastToken);
            strcat(path, "/");
            token = strtok(NULL, "/");
        }
    } 

    // write the file
    if (fp == NULL) {
        fp = fopen(fname, "wb");
    }
    if (fp == NULL) {
        char* response = constructPOSTResponse(RESPONSE_BODY_POST_ERR);
        write(sock, response, strlen(response)-1);
        free(response);
        return;
    }
    fwrite(body, sizeof(char), contentLength, fp);
    fclose(fp);
    char* response = constructPOSTResponse(RESPONSE_BODY_POST_OK);
    write(sock, response, strlen(response)-1);
    free(username);
    free(response);
}

static void login(int sock, char *path) {
    char* token = strtok(path, "=");
    token = strtok(NULL, "&");
    char* username = strndup(token, 128);
    token = strtok(NULL, "=");
    token = strtok(NULL, "\n");
    char* password = strndup(token, 128);
    
    FILE* fp = fopen("users", "r");
    if (fp == NULL) {
        perror("no users file found");
    }

    char buffer[258]; // 258 = 128 + 1 + 128 + 1
    while (fgets(buffer, 258, fp)) {
        char* token = strtok(buffer, ":");
        char* usernameRecord = strndup(token, 128);
        token = strtok(NULL, "\n");
        char* passwordRecord = strndup(token, 128);
        if (strncmp(username, usernameRecord, 128) == 0 &&
                strncmp(password, passwordRecord, 128) == 0) {
            // found a match => generate an auth token
            FILE* fpc = fopen("cookies", "a");
            if (fpc == NULL) {
                perror("no cookies file found");
            }
            char authToken[32];
            genRandom(authToken, 32);
            fprintf(fpc, "%s:%s\n", username, authToken);
            fclose(fpc);
            char* response = constructLoginResponse(RESPONSE_BODY_POST_OK, authToken);
            write(sock, response, strlen(response)-1);
            free(response);
            break;
        }
    }
}

// read a binary file and write it to a socket  
static void readFile(int sock, char *fname, char *cookie) {
    char *username = malloc(128);
    getUsername(username, fname);

    // return early if this isn't a valid user / cookie combo
    if (!authenticated(sock, username, cookie)) {
        free(username);
        return;
    }

    int fd;
    int bytes;
    void *buffer[BYTES];
    if ((fd = open(fname, O_RDONLY)) != -1) {
        while ((bytes = read(fd, buffer, BYTES)) > 0) {
            write(sock, buffer, bytes);
        }
   } else {
        char* response = constructHTTP404();
        write(sock, response, strlen(response)-1);
        free(response);
   }
   free(username);
}

void httpRequest(int sock, char *request) { 
    printf("%s\n----------\n", request);
    char method[6];
    char path[BYTES];
    char version[16];
    sscanf(request, "%6s %2048s %16s\n", method, path, version);

    // user tries to access a relative path
    if (strstr(path, "..") != NULL) {
        char* response = constructHTTP401();
        write(sock, response, strlen(response)-1);
        free(response); 
        return;
    }

    // GET requests
    if (strncmp(method, GET, strlen(GET)) == 0) {
        // Out of scope for this assignment, but expected for web servers
        // if (strlen(path) == 1 && path[0] == '/') {
        //     strncpy(path, "index.html", BYTES);
        // }

        if (strncmp(path, "/", 1) == 0) {
            memmove(path, path+1, BYTES); 
        }
        char* cookie = getCookie(request);
        if (strnlen(cookie, 32) == 0) {
            perror("failed to grab cookie");
        }
        readFile(sock, path, cookie);
        free(cookie);

    // POST requests
    } else if (strncmp(method, POST, strlen(POST)) == 0) {
        if (strncmp(path, "/", 1) == 0) {
            memmove(path, path+1, BYTES); 
        }

        // handle login attempts
        if (strncmp(path, LOGIN_ENDPOINT, strlen(LOGIN_ENDPOINT)) == 0) {
            login(sock, path);
        }

        unsigned int contentLength = getContentLength(request);
        if (contentLength == 0) {
            char* response = constructHTTP401();
            write(sock, response, strlen(response)-1);
            free(response); 
            return;
        }
        char* cookie = getCookie(request);
        if (strnlen(cookie, 32) == 0) {
            return;
        }
        char* body = strstr(request, "\r\n\r\n");
        if (body == NULL) {
            return;
        }
        memmove(body, body+4, contentLength);
        body[contentLength] = '\0';
        writeFile(sock, path, body, contentLength, cookie);
        free(cookie);
    } else {
    }
}
