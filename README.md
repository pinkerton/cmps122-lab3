# CMPS 122 Lab 3: Part 3



## Useful commands

POST a file to the server with:

`curl --data-binary @file.bin localhost:7000/uploaded_file`

Include a cookie in requests with:

`curl -v -b "authToken=6zhpYEbz" "localhost:7000/foo"`
